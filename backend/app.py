from flask import Flask, request, jsonify
from flask_cors import CORS
from flask_jwt_extended import JWTManager, create_access_token, jwt_required, get_jwt_identity
from flask_jwt_extended.exceptions import JWTExtendedException

app = Flask(__name__)
app.config['SECRET_KEY'] = 'marlon_sensie'
app.config['JWT_SECRET_KEY'] = 'marlon_sensie'
jwt = JWTManager(app)
cors = CORS(app)

import sqlite3

# Custom error handlers
@jwt.unauthorized_loader
def unauthorized_callback(error):
    return jsonify({'message': 'Unauthorized', 'error': 'invalid_token'}), 401

@jwt.expired_token_loader
def expired_token_callback(expired_token, sample):
    return jsonify({'message': 'Token has expired', 'error': 'expired_token'}), 401

@jwt.invalid_token_loader
def invalid_token_callback(error):
    return jsonify({'message': 'Invalid token', 'error': 'invalid_token'}), 401

@jwt.revoked_token_loader
def revoked_token_callback(revoked_token):
    return jsonify({'message': 'Token has been revoked', 'error': 'revoked_token'}), 401
# Custom error handlers end

def get_db_connection():
	return sqlite3.connect('backend/database.db')

# Replace this with proper user registration and password hashing logic
@app.route('/register', methods=['POST'])
def register():
	data = request.get_json()
	username = data.get('username')
	password = data.get('password')
	if not username or not password:
	    return jsonify({'message': 'Username and password are required'}), 400

	# You should hash the password before saving it to the database
	connection = get_db_connection()
	cursor = connection.cursor()
	cursor.execute('INSERT INTO users (username, password) VALUES (?, ?)', (username, password))
	connection.commit()
	connection.close()

	return jsonify({'message': 'User registered successfully'}), 201

# Login route
@app.route('/login', methods=['POST'])
def login():
	data = request.get_json()
	username = data.get('username')
	password = data.get('password')

	# Fetch the user from the database based on the username
	connection = get_db_connection()
	cursor = connection.cursor()
	cursor.execute('SELECT * FROM users WHERE username = ?', (username,))
	user = cursor.fetchone()
	connection.close()

	if user and user[2] == password:  # Replace this with proper password verification
	    access_token = create_access_token(identity=username)
	    return jsonify({'access_token': access_token}), 200
	else:
	    return jsonify({'message': 'Invalid credentials'}), 401

# Protected route
@app.route('/protected', methods=['GET'])
@jwt_required()
def protected():
    current_user = get_jwt_identity()
    return jsonify({'message': 'This is a protected route', 'user': current_user}), 200

if __name__ == '__main__':
    app.run(debug=True)
    # app.run(debug=True, host='10.0.254.7', port=5000)